require qt5.inc
require qt5-lts.inc

LICENSE = "GPL-3.0-only | The-Qt-Company-Commercial"
LIC_FILES_CHKSUM = " \
    file://LICENSE.GPL3;md5=d32239bcb673463ab874e80d47fae504 \
"

DEPENDS += "qtbase qtdeclarative qtmultimedia qtxmlpatterns"

SRCREV = "1cce5c9438488b0501ee4849cd41c98d122617d0"
